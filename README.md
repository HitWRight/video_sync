

# Aprašymas

Video files synchronizer.  
Has 2 states.  
The initial check.  
The filesystem watch.  

On the initial check connects to every screen and compares every single video.  
If any not the same adds them to the send list.  
Adds file system watchers to track if anything changes inside the folders.  
If a video propogates them to the screens.  

SMB client mounts each screen.  


# Reikalavimai

-   rustc kompiliatorius
-   cargo
-   SMB client


# Kompiliavimas

-   Atsidaromas katalogas per CMD/PS/Terminal
-   paleidžiama "cargo build" komanda


# Instaliavimas

-   Nukopijuojamas executable failas


# Testavimas

Nėra  


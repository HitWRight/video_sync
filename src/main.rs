use aurora_client::client::screen::ScreenCalls;
use clap::Parser;
use log::{error, info, warn};
use notify::event::EventKind;
use notify::RecursiveMode;
use notify::Watcher;
use std::path::PathBuf;

mod uploader;
mod send;

#[derive(Parser)]
struct Opts {
    ///Aurora API hostname address
    #[clap(short = 's', long = "server", default_value = "http://127.0.0.1:3816")]
    aurora_api_host: String,
    ///Aurora API user id
    #[clap(long, default_value = "test")]
    aurora_api_user: String,
    ///Aurora API key
    #[clap(long, default_value = "test")]
    aurora_api_key: String,
    ///UserId
    #[clap(long, default_value = "1")]
    aurora_user_id: i32,
    ///Aurora upload server url
    #[clap(long, default_value = "http://127.0.0.1:8000")]
    aurora_upload_server_url: String,
    ///Aurora upload server key
    #[clap(long, default_value = "test_key")]
    aurora_upload_server_key: String,
    ///Wait time after failed send/download
    #[clap(long, default_value = "300")]
    retry_delay: u64,
    /// Sets whether the hidden files (files that start with a dot) should be included
    #[clap(short, long)]
    include_hidden: bool,
}

#[tokio::main]
async fn main() -> ! {
    let opts: Opts = Opts::parse();
    env_logger::init();
    info!("Retrieving screen data from {}", opts.aurora_api_host);
    let client = aurora_client::client::Client::new(
        &opts.aurora_api_user,
        &opts.aurora_api_key,
        &opts.aurora_api_host,
        opts.aurora_user_id,
    );

    let screen_logins = tokio_retry::Retry::spawn(
        tokio_retry::strategy::FixedInterval::from_millis(30000).take(6),
        || async { ScreenCalls::get_screen_logins(&client).await },
    )
    .await
    .unwrap();

    // create structure if doesn't exist
    info!("Creating directories for uploads");
    let mut directories = screen_logins
        .clone()
        .into_iter()
        .map(|screen| format!("upload/{}", screen.video_version))
        .collect::<Vec<String>>();
    directories.sort();
    directories.dedup();
    for dir in &directories {
        std::fs::create_dir_all(&dir)
            .unwrap_or_else(|err| panic!("Failed to create directory ({}): {:?}", dir, err));
        info!("Directory created: {}", dir);
    }
    let directories: Vec<PathBuf> = directories.into_iter().map(|d| PathBuf::from(&d)).collect();

    // add listeners on file system before main sync not to lose file edits
    let (send, receive) = std::sync::mpsc::channel();
    let mut watcher = notify::recommended_watcher(send).unwrap();
    for dir in &directories {
        watcher
            .watch(dir, RecursiveMode::NonRecursive)
            .unwrap_or_else(|err| {
                panic!(
                    "Failed to hook up watcher to directory ({:?}): {:?}",
                    dir, err
                )
            });
    }

    // create uploaders
    let mut uploaders: Vec<_> = screen_logins
        .into_iter()
        .map(|s| uploader::Uploader::new(s, opts.retry_delay))
        .collect();

    //Start upload tasks
    let _handles: Vec<_> = uploaders.iter_mut().map(|u| u.start_send_task()).collect();

    //Send all current files
    for dir in &directories {
        info!("Synchronising directory: {:?}", dir);
        let files = std::fs::read_dir(dir)
            .unwrap_or_else(|err| panic!("Failed to read directory ({:?}): {:?}", dir, err))
            .map(|f| f.unwrap().path())
            .collect::<Vec<PathBuf>>();

        for upload in &uploaders {
            let sender = upload.get_send();
            if dir
                .to_string_lossy()
                .contains(&upload.get_video_version().to_string())
            {
                for file in files.clone() {
                    sender.send(file).unwrap();
                }
            }
        }
    }


    //Start video downloader task
    let _ = tokio::task::spawn(async move {
        let client = reqwest::Client::builder().build().unwrap();
        loop {
            let result = client
                .post(format!(
                    "{}/aurora/videos/next",
                    opts.aurora_upload_server_url
                ))
                .form(&[("key", &opts.aurora_upload_server_key)])
                .send()
                .await
                .unwrap();
            println!("{:?}", result);

            if result.status().is_success() {
                let path = PathBuf::from(result.url().path());
                let filename = path.file_name().unwrap().to_str().unwrap();
                let filepath = PathBuf::from(format!("./upload/{}", filename));
                let mut file = std::fs::File::create(filepath.clone()).unwrap();
                let mut content = std::io::Cursor::new(result.bytes().await.unwrap());
                std::io::copy(&mut content, &mut file).unwrap();

                for directory in &directories {
                    dbg!(directory);
                    std::fs::copy(&filepath, directory.join(filename)).unwrap();
                }
                std::fs::remove_file(filepath).unwrap();
                continue;
            }

            tokio::time::sleep(std::time::Duration::from_secs(opts.retry_delay)).await;
        }
    });

    //start file upload tracking loop
    loop {
        match receive.recv() {
            Ok(Ok(event)) => match event.kind {
                EventKind::Create(_) | EventKind::Modify(_) => {
                    for path in event.paths {
                        for upload in &uploaders {
                            if path
                                .to_string_lossy()
                                .contains(&upload.get_video_version().to_string())
                            {
                                upload.get_send().send(path.clone()).unwrap();
                            }
                        }
                    }
                }
                _ => warn!("Unknown event: {:?}", event),
            },
            Ok(Err(e)) => error!("Watch error: {:?}", e),
            Err(e) => error!("Send error: {:?}", e),
        }
    }
}

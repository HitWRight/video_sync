use aurora_client::models::screen::ScreenLogin;
use log::{debug, error, info, warn};
use std::path::{Path, PathBuf};

#[derive(Clone, Debug)]
pub struct SendFile {
    pub file: PathBuf,
    pub screen: ScreenLogin,
}

#[derive(Debug)]
pub enum SendError {
    ConnectionFailed(SendFile),
}

pub async fn send_file(send_file: SendFile) -> Result<(), SendError> {
    if send_file
        .file
        .file_name()
        .unwrap()
        .to_str()
        .unwrap()
        .starts_with('.')
    {
        info!(
            "Ignoring file: {}",
            send_file.file.file_name().unwrap().to_str().unwrap()
        );
        return Ok(());
    }

    info!(
        "Sending file {} to {}",
        send_file.file.file_name().unwrap().to_str().unwrap(),
        send_file.screen.name
    );

    //check if dir exists otherwise create
    let mount_dir = format!("/mnt/screen/{}", send_file.screen.name);
    if let Err(error_message) = std::fs::create_dir_all(&mount_dir) {
        error!(
            "Failed to create directory ({}): {:?}",
            mount_dir, error_message
        );
        return Err(SendError::ConnectionFailed(send_file));
    }
    let mount_dir = Path::new(&mount_dir);
    if !block_utils::is_mounted(mount_dir).unwrap() {
        info!("Mounting share");
        let mount_result = std::process::Command::new("mount")
            .arg("-t")
            .arg("cifs")
            .arg("-o")
            .arg(format!(
                "domain=samba,username={},password={}",
                send_file.screen.username, send_file.screen.password
            ))
            .arg(format!("//{}/Darbas/", send_file.screen.ipv4))
            .arg(mount_dir.to_str().unwrap())
            .output()
            .unwrap();
        debug!("{:?}", mount_result);

        if !mount_result.status.success() {
            error!(
                "Failed to mount ({}): {:?}",
                mount_dir.to_str().unwrap(),
                mount_result
            );
            return Err(SendError::ConnectionFailed(send_file));
        }
    }

    let path = format!(
        "{}/Klipai/{}",
        mount_dir.to_str().unwrap(),
        send_file.file.file_name().unwrap().to_str().unwrap()
    );
    let path = Path::new(&path);

    let source_file_size = match tokio::fs::metadata(send_file.file.clone()).await {
        Ok(metadata) => metadata.len(),
        Err(_) => 0,
    };

    let destination_file_size = match tokio::fs::metadata(path).await {
        Ok(metadata) => metadata.len(),
        Err(_) => 0,
    };
    if source_file_size != destination_file_size {
        if path.exists() {
            //TODO: check metadata if really that is necessary
            warn!("File exists removing!");
            match tokio::fs::remove_file(path).await {
                Ok(()) => {}
                Err(error_message) => {
                    error!("Failed to remove file: {:?}", error_message);

                    return Err(SendError::ConnectionFailed(send_file));
                }
            };
        }

        match tokio::fs::copy(send_file.file.clone(), path).await {
            Ok(_) => info!("File copied successfully '{:?}' to {}", send_file.file, send_file.screen.name),
            Err(error_message) => {
                error!("Failed to copy file: {:?}", error_message);
                return Err(SendError::ConnectionFailed(send_file));
            }
        };
    }

    Ok(())
}

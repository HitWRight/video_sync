use aurora_client::models::screen::ScreenLogin;
use aurora_client::models::video_version::VideoVersion;
use log::{debug, error};
use std::path::PathBuf;
use tokio::sync::mpsc::{UnboundedReceiver, UnboundedSender};
use tokio::task::JoinHandle;

pub struct Uploader {
    login: ScreenLogin,
    recv: Option<UnboundedReceiver<PathBuf>>,
    retry_timeout: u64,
    send: UnboundedSender<PathBuf>,
}

impl Uploader {
    pub fn new(login: ScreenLogin, retry_timeout: u64) -> Uploader {
        let (send, recv) = tokio::sync::mpsc::unbounded_channel();
        Uploader {
            login,
            recv: Some(recv),
            retry_timeout,
            send,
        }
    }

    pub fn get_video_version(&self) -> &VideoVersion {
        &self.login.video_version
    }

    pub fn get_send(&self) -> &UnboundedSender<PathBuf> {
        &self.send
    }

    pub fn start_send_task(&mut self) -> JoinHandle<()> {
        let send = self.send.clone();
        if self.recv.is_none() {
            panic!("Can't start a task that was started before");
        }
        let recv = std::mem::replace(&mut self.recv, None);
        let mut recv = recv.unwrap();
        let login = self.login.clone();
        let retry_timeout = self.retry_timeout;
        tokio::task::spawn(async move {
            loop {
                match recv.recv().await {
                    Some(file) => {
                        debug!("Sending: {:?}", file);
                        let screen_queue_item = crate::send::SendFile {
                            file: file.clone(),
                            screen: login.clone(),
                        };
                        let send = send.clone();
                        let _ = tokio::task::spawn(async move {
                            match crate::send::send_file(screen_queue_item.clone()).await {
                                Err(e) => {
                                    error!("Error found: {:?}... retrying!", e);
                                    tokio::time::sleep(std::time::Duration::from_secs(
                                        retry_timeout,
                                    ))
                                    .await;
                                    send.send(file).unwrap();
                                }
                                _ => {}
                            }
                        });
                    }
                    None => {
                        break;
                    }
                }
            }
        })
    }
}
